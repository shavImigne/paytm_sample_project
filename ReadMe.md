# PayTM Sample Project - Working with spring boot 

## Project Functionalities: 
1. Hibernate entity "Student" with 3 attriutes 
2. GET api to return list of all students in database. 
3. POST api to accept a studen object using @RequestBody and saving in in-memory array list. 
4. GET api with substring search for name and return students that have the substring in their name.
5. DELETE api to delete a record from the database. 
6. Connecting the entity to local SQL database. 

## API'S USED: 
1. org.springframework.web.bind.annotation.DeleteMapping
2. org.slf4j.Logger
3. org.slf4j.LoggerFactory
4. org.springframework.web.bind.annotation.GetMapping
5. org.springframework.web.bind.annotation.PathVariable
6. org.springframework.web.bind.annotation.PostMapping
7. org.springframework.web.bind.annotation.PutMapping
8. org.springframework.web.bind.annotation.RequestBody
9. org.springframework.web.bind.annotation.RestController

