package com.example.demo;

import org.springframework.data.repository.CrudRepository;

interface StudentRepo extends CrudRepository<Student, Integer> {

}