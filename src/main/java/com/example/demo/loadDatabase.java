package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class loadDatabase {

	  private static final Logger log = LoggerFactory.getLogger(loadDatabase.class);

	  @Bean
	  CommandLineRunner initDatabase(StudentRepo repository) {

	    return args -> {
	      log.info("Preloading " + repository.save(new Student(1,"Ian arsdon", 11)));
	      log.info("Preloading " + repository.save(new Student(2,"Shav Imigne", 10)));
	    };
	  }
}
