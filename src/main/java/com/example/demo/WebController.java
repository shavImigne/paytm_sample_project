package com.example.demo;

import org.springframework.web.bind.annotation.DeleteMapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
public class WebController{
	private final StudentRepo repository;
	
	WebController(StudentRepo Inrepository){
		this.repository = Inrepository;
	}
	
	 @GetMapping("/students")
	  List<Student> all() {
	    return (List<Student>) repository.findAll();
	  }	
	  // end::get-aggregate-root[]

	 @GetMapping("/studentsSub/{substr}")
	  List<Student> matchSubstrings(@PathVariable String substr) {
		 List<Student> ret = new ArrayList<Student>();
		 final Logger log = LoggerFactory.getLogger(loadDatabase.class);
		 List<Student> allStudents;
		 log.info("substring to check: " + substr);
		 allStudents = (List<Student>) repository.findAll();
		 for(int i=0;i<allStudents.size();i++) {
			 	String name = allStudents.get(i).getName();
			 	if(name.contains(substr)) {
			 		ret.add(allStudents.get(i));
			 	}
		 }
		 return ret;
	  }	
	 
	  @PostMapping("/students")
	  String newStudent(@RequestBody Student newStudent) {
		  repository.save(newStudent);
		  return "Entry saved";
	  }
	  
	  @DeleteMapping("/students/{id}")
	  void deleteStudent(@PathVariable Integer id) {
	    repository.deleteById(id);
	  }

}