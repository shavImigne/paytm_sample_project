package com.example.demo;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Student {
	private @Id @GeneratedValue Integer StudRoll;
	private String StudName;
	private int grade;
	
	Student(){
		this.StudRoll =0;
		this.StudName = "";
		this.grade = 0;
	}
	Student(Integer inRoll,String inName, int inStd){
		this.StudRoll = inRoll;
		this.StudName = inName;
		this.grade = inStd;
	}
	public Integer getRoll() {
		return this.StudRoll;
	}
	public String getName() {
		return this.StudName;
	}
	public int getGrade() {
		return this.grade;
	}
	public void setRoll(int inRoll) {
		this.StudRoll = inRoll;
	}
	public void setName(String inName) {
		this.StudName = inName;
	}
	public void setGrade(int inGrade) {
		this.grade = inGrade;
	}
	
}
